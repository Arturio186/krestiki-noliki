class GameField {
    constructor() {
        this.state = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];
        this.isOverGame = false;
        this.mode = 'X';
    }

    getGameFieldStatus() {
        let winSymbol = 0; 
        let areaSize = 3;

        for (let i = 0; i < areaSize; i++)
        {
            const rowFirstSymbol = area[i][0];
            winSymbol = rowFirstSymbol;

            if (rowFirstSymbol !== null)
            {
                for (let j = 1; j < areaSize; j++)
                {
                    if (area[i][j] !== rowFirstSymbol)
                    {
                        winSymbol = 0;
                        break;
                    }
                }
                if (winSymbol !== 0)
                    return [winSymbol, 'horizontal'];
            }

            const columnFirstSymbol = area[0][i];
            winSymbol = columnFirstSymbol;

            if (columnFirstSymbol !== null)
            {
                for (let j = 1; j < areaSize; j++)
                {
                    if (area[j][i] !== columnFirstSymbol)
                    {
                        winSymbol = 0;
                        break;
                    }
                }
                if (winSymbol !== 0)
                    return [winSymbol, 'vertical'];
            }       
        }

        // Левый верхний угол, левый нижний угол | Символы
        let corners = [area[0][0], area[areaSize-1][0]]; 

        for (let i = 1; i < areaSize; i++)
        {
            if (corners[0] !== null)
            {
                if (area[i][i] !== corners[0])
                    corners[0] = null;
            }
            if (corners[1] !== null)
            {
                if (area[areaSize-i-1][i] !== corners[1])
                    corners[1] = null;
            }
        }

        for (let i = 0; i < corners.length; i++)
        {
            if (corners[i] !== null)
                return [corners[i], 'diagonal'];
        }

        const CheckDraw = () => {
            for (let i = 0; i < areaSize; i++)
            {
                for (let j = 0; j < areaSize; j++)
                {
                    if (area[i][j] == null)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        let isDraw = CheckDraw();

        if (isDraw)
            return [0, 'draw']

        return winSymbol;
    }

    printGameArea() {
        let output = "";
        for (let i = 0; i < 3; i++)
        {
            for (let j = 0; j < 3; j++)
            {
                output += `${this.state[i][j]} `;
            }
            output += '\n';
        }
        alert(output);
    }

    getNextPlayer() {
        let result = this.mode === 'X' ? 'O' : 'X';

        return result;
    }

    setMode(newMode) {
        if (newMode === 'X' || newMode === 'O')
            this.mode = newMode;
        else 
            alert(`Емае... Я могу присвоить только 'X' или 'O' полю mode.\nВы присваиваете значение ${newMode}`);
    }

    setFieldCellValue(value) {
        let x = prompt(`Ходит ${this.mode}\nВведите номер строки`);
        if (x < 1 || x > 3)
        {
            alert("Некорректные координаты");
            return false;
        }

        let y = prompt(`Ходит ${this.mode}\nВведите номер стобца`);
        if (y < 1 || y > 3)
        {
            alert("Некорректные координаты");
            return false;
        }


        if (this.state[x-1][y-1] === null)
        {
            this.state[x-1][y-1] = value;
            return true;
        }
        else
        {
            alert("Клетка уже занята");
            return false;
        }
            
    }
}

const gameField = new GameField();
let gameRes = 0;

while(!gameField.isOverGame) 
{
    if (!gameField.setFieldCellValue(gameField.mode))
        continue;

    gameRes = gameField.getGameFieldStatus();
    
    if (gameRes !== 0)
    {
        this.isOverGame = true;
        break;
    }
        
    gameField.setMode(gameField.getNextPlayer());

    gameField.printGameArea();
}

switch (gameRes[1])
{
    case 'horizontal':
        console.log(`Выиграл ${gameRes[0]}, т.к. есть 3 заполненные клетки с ${gameRes[0]} по горизонатли`);
        break;
    case 'vertical':
        console.log(`Выиграл ${gameRes[0]}, т.к. есть 3 заполненные клетки с ${gameRes[0]} по вертикали`);
        break;
    case 'diagonal':
        console.log(`Выиграл ${gameRes[0]}, т.к. есть 3 заполненные клетки с ${gameRes[0]} по диагонали`);
        break;
    case 'draw':
        console.log('Ничья');
        break;
}