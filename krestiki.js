/* Тестировал 4х4 поле, возможность расширения поля с этим кодом существует 
нужно лишь поменять areaSize
gameField = [
    ['x', null, 'o', 'o'],
    ['o', 'x', 'o', null],
    ['o', 'o', 'x', null],
    ['o', null, null, null]
];
*/
const gameField = [
    ['x', 'o', 'x'],
    ['x', 'o', 'o'],
    ['o', 'x', 'x']
];


const gameResult = (area) => {
    let winSymbol = 0; 
    let areaSize = 3;

    for (let i = 0; i < areaSize; i++)
    {
        const rowFirstSymbol = area[i][0];
        winSymbol = rowFirstSymbol;

        if (rowFirstSymbol !== null)
        {
            for (let j = 1; j < areaSize; j++)
            {
                if (area[i][j] !== rowFirstSymbol)
                {
                    winSymbol = 0;
                    break;
                }
            }
            if (winSymbol !== 0)
                return [winSymbol, 'horizontal'];
        }

        const columnFirstSymbol = area[0][i];
        winSymbol = columnFirstSymbol;

        if (columnFirstSymbol !== null)
        {
            for (let j = 1; j < areaSize; j++)
            {
                if (area[j][i] !== columnFirstSymbol)
                {
                    winSymbol = 0;
                    break;
                }
            }
            if (winSymbol !== 0)
                return [winSymbol, 'vertical'];
        }       
    }

    // Левый верхний угол, левый нижний угол | Символы
    let corners = [area[0][0], area[areaSize-1][0]]; 

    for (let i = 1; i < areaSize; i++)
    {
        if (corners[0] !== null)
        {
            if (area[i][i] !== corners[0])
                corners[0] = null;
        }
        if (corners[1] !== null)
        {
            if (area[areaSize-i-1][i] !== corners[1])
                corners[1] = null;
        }
    }

    for (let i = 0; i < corners.length; i++)
    {
        if (corners[i] !== null)
            return [corners[i], 'diagonal'];
    }

    const CheckDraw = () => {
        for (let i = 0; i < areaSize; i++)
        {
            for (let j = 0; j < areaSize; j++)
            {
                if (area[i][j] == null)
                {
                    return false;
                }
            }
        }
        return true;
    }

    let isDraw = CheckDraw();

    if (isDraw)
        return [0, 'draw']

    return winSymbol;
}

let gameState = gameResult(gameField);

if (gameState === 0)
{
    console.log('Игра продолжается');
}
else 
{
    switch (gameState[1])
    {
        case 'horizontal':
            console.log(`Выиграл ${gameState[0]}, т.к. есть 3 заполненные клетки с ${gameState[0]} по горизонатли`);
            break;
        case 'vertical':
            console.log(`Выиграл ${gameState[0]}, т.к. есть 3 заполненные клетки с ${gameState[0]} по вертикали`);
            break;
        case 'diagonal':
            console.log(`Выиграл ${gameState[0]}, т.к. есть 3 заполненные клетки с ${gameState[0]} по диагонали`);
            break;
        case 'draw':
            console.log('Ничья');
            break;
    }
}